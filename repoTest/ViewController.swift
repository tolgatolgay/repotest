//
//  ViewController.swift
//  repoTest
//
//  Created by Tolga Tolgay on 17.03.2019.
//  Copyright © 2019 Tolga Tolgay. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var myTextField: UITextField!
    var count = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func buttenPressed(_ sender: UIButton) {
        print ("Button Pressed")
        myTextField.text = "Button Pressed! Wohoo!"
        count += 1
        countLabel.text = "\(count)"
        //countLabel.text = "AMK"
    }
}

